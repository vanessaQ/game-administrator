/* INICIO variables */ 
    let users = [
    ]
    let playerEdit = {
        id: null,
        character: null,
        skin: null,
        nickname: null,
    }
    let selectedDisplay = null;
    let userUp1 = null;
    let userUp2 = null;

    let score1 = 0;
    let score2 = 0;

    let editUser = null;

/* FIN variables */ 

/* INICIO inicializadores */ 

if( users.length > 0 ){
    for( let i = 0; i < users.length; i++){
        let playerSpan = document.getElementById(`players${i+1}`)
        let playerHiddenInput = document.getElementById(`playersName${i+1}`)
        playerSpan.innerHTML = users[i].nickname
        playerHiddenInput.value = users[i].nickname
    }
}

$('#modalPlayerCharacterSelect').select2().on('select2:open',function(){
    $('.select2-dropdown--above').attr('id','fix');
    $('#fix').removeClass('select2-dropdown--above');
    $('#fix').addClass('select2-dropdown--below');
});

$('#modalPlayerCharacterSelectEdit').select2().on('select2:open',function(){
    $('.select2-dropdown--above').attr('id','fix2');
    $('#fix2').removeClass('select2-dropdown--above');
    $('#fix2').addClass('select2-dropdown--below');
});

let modal = document.getElementById("ventanaModal");
let spanX = document.getElementsByClassName("cerrar")[0];
let addUserButton = document.getElementById("addUserButton")
spanX.addEventListener("click",function() {
    modal.style.display = "none";
});

addUserButton.addEventListener("click",function() {
    addUser()
});

let modalScore = document.getElementById("ventanaModalScore");
let spanXScore = document.getElementById("cerrarScore");
spanXScore.addEventListener("click",function() {
    modalScore.style.display = "none";
});

let modalEdit = document.getElementById('ModalEdit');
let cerrarEdit = document.getElementById("cerrarEdit");
let editUserButton = document.getElementById("editUserButton")
let deactivateUserButton = document.getElementById("deactivateUser")
let activateUserButton = document.getElementById("activateUser")

cerrarEdit.addEventListener("click",function() {
    modalEdit.style.display = "none";
});
editUserButton.addEventListener("click",function() {
    saveUser()
});
deactivateUserButton.addEventListener("click",function() {
    deactivateUser()
});
activateUserButton.addEventListener("click",function() {
    activateUser()
});


  // Si el usuario hace clic fuera de la ventana, se cierra.
window.addEventListener("click",function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
});

let videos = document.getElementsByClassName('video');
for( let i = 0; i < videos.length; i++ ){
    videos[i].play()
}

let videosContainer = document.getElementsByClassName('videoContainer');
for( let i = 0; i < videosContainer.length; i++ ){
    videosContainer[i].addEventListener("click", e => {
        verifyUserStatus(i, e)
    })
}

let scoreContainer = document.getElementById('score');
scoreContainer.addEventListener("click", e => {
    openModalScore()
})

let spanScore1 = document.getElementById('scoreP1')
spanScore1.innerHTML = score1;

let spanScore2 = document.getElementById('scoreP2')
spanScore2.innerHTML = score2;


$('#modalPlayerCharacterSelect').on('select2:select', function (e) {
    selectCharacter(e.params.data)
});

$('#modalPlayerCharacterSelectEdit').on('select2:select', function (e) {
    selectCharacterEdit(e.params.data)
});

document.getElementById('modalSelectSkin').addEventListener("change", (event) => setSkin(event));
document.getElementById('modalSelectSkinEdit').addEventListener("change", (event) => setSkinEdit(event));

let reduceButton1 = document.getElementById('reduceScore1');
let reduceButton2 = document.getElementById('reduceScore2')
let addButton1 = document.getElementById('addScore1');
let addButton2 = document.getElementById('addScore2')
let scoreModalSpan1 = document.getElementById('scoreModalSpan1')
let scoreModalSpan2 = document.getElementById('scoreModalSpan2')

reduceButton1.addEventListener("click", () => {
    if( score1 > 0 ){
        score1--;
    }else{
        score1 = 40
    }
    spanScore1.innerHTML = score1;
    scoreModalSpan1.innerHTML = score1;
})

addButton1.addEventListener("click", () => {
    if( score1 < 40 ){
        score1++;
    }else{
        score1 = 0;
    }
    spanScore1.innerHTML = score1;
    scoreModalSpan1.innerHTML = score1;
})


reduceButton2.addEventListener("click", () => {
    if( score2 > 0 ){
        score2--;
    }else{
        score2 = 40
    }
    spanScore2.innerHTML = score2;
    scoreModalSpan2.innerHTML = score2;
})

addButton2.addEventListener("click", () => {
    if( score2 < 40 ){
        score2++;
    }else{
        score2 = 0;
    }
    spanScore2.innerHTML = score2;
    scoreModalSpan2.innerHTML = score2;
})

let squareUser1 = document.getElementById('player1');
squareUser1.addEventListener("click", () => {
    document.getElementById('modalPlayerNameEdit').value = userUp1.nickname
    $("#modalPlayerCharacterSelectEdit").val(userUp1.character).trigger('change')
    let img = document.getElementById('modalImageCharacterEdit');
    img.src=`./resources/assets/crew/Chars/${userUp1.character}/Skins/${userUp1.skin}`
    img.style.display = 'flex';
    editUser = 1;
    if( userUp1.active == true ) {
        activateUserButton.style.display = 'none';
        deactivateUserButton.style.display = 'block';
    }
    else {
        deactivateUserButton.style.display = 'none';
        activateUserButton.style.display = 'block';
    }
    modalEdit.style.display = "block";
})

let squareUser2 = document.getElementById('player2');
squareUser2.addEventListener("click", () => {
    document.getElementById('modalPlayerNameEdit').value = userUp2.nickname
    $("#modalPlayerCharacterSelectEdit").val(userUp2.character).trigger('change')
    let img = document.getElementById('modalImageCharacterEdit');
    img.src=`./resources/assets/crew/Chars/${userUp2.character}/Skins/${userUp2.skin}`
    img.style.display = 'flex';
    editUser = 2;
    if( userUp2.active == true ) deactivateUserButton.style.display = 'hidden';
    else deactivateUserButton.style.display = 'none';
    modalEdit.style.display = "block";
})


/* FIN inicializadores */ 

/* INICIO metodos */

const cleanOptions = ( edit = false ) => {
    let selectElement = null;
    if( edit == false ){
        selectElement = document.getElementById('modalSelectSkin')
    }else{ 
        selectElement = document.getElementById('modalSelectSkinEdit')
    }
    var i, L = selectElement.options.length - 1;
    for(i = L; i >= 0; i--) {
        selectElement.remove(i);
    }
}

const cleanOptionsEdit = () => {
    let selectElement = document.getElementById('modalSelectSkinEdit')
    var i, L = selectElement.options.length - 1;
    for(i = L; i >= 0; i--) {
        selectElement.remove(i);
    }
}

const addUser = () => {
    let nickname = document.getElementById('modalPlayerName').value
    let characterSelect = document.getElementById('modalPlayerCharacterSelect')
    let selected = characterSelect.options[characterSelect.selectedIndex].value;
    let skinSelect = document.getElementById("modalSelectSkin");
    let selectedSkin = skinSelect.options[skinSelect.selectedIndex].value;

    let newUser = {
        nickname: nickname,
        character: selected,
        skin: selectedSkin,
        id: selectedDisplay,
        active: true
    }

    users.push(newUser)

    let playerSpan = document.getElementById(`players${selectedDisplay}`)
    let playerHiddenInput = document.getElementById(`playersName${selectedDisplay}`)
    playerSpan.innerHTML = nickname
    playerHiddenInput.value = nickname

    selectedDisplay = null;
    document.getElementById('modalPlayerName').value = ''
    $("#modalPlayerCharacterSelect").val('0').trigger('change')
    cleanOptions()
    let img = document.getElementById('modalImageCharacter');
    img.src=''
    img.style.display = 'none';
    modal.style.display = "none";
}


const saveUser = () => {
    let nickname = document.getElementById('modalPlayerNameEdit').value
    let characterSelect = document.getElementById('modalPlayerCharacterSelectEdit')
    let selected = characterSelect.options[characterSelect.selectedIndex].value;
    let skinSelect = document.getElementById("modalSelectSkinEdit");
    let selectedSkin = skinSelect.options[skinSelect.selectedIndex].value;

    id = null;
    if( editUser == 1 ){
        id = userUp1.id
    }else{
        id = userUp2.id
    }

    let user = users.find((item) => item.id == id )
    user.nickname = nickname;
    user.character = selected;
    if(selectedSkin == '0') user.skin = `${selected}${1}.png`
    else user.skin = selectedSkin;
    user.active = true;

    let index = users.indexOf(user)
    users[index] = user;

    let playerSpan = document.getElementById(`players${user.id}`)
    let playerHiddenInput = document.getElementById(`playersName${user.id}`)
    playerSpan.innerHTML = nickname
    playerHiddenInput.value = nickname

    selectedDisplay = null;
    document.getElementById('modalPlayerNameEdit').value = ''
    $("#modalPlayerCharacterSelectEdit").val('0').trigger('change')
    cleanOptions(true)
    let img = document.getElementById('modalImageCharacterEdit');
    img.src=''
    img.style.display = 'none';
    modalEdit.style.display = "none";
    editUser = null;
    handleUser(id-1);
}

const deactivateUser = () => {

    id = null;
    if( editUser == 1 ){
        id = userUp1.id
    }else{
        id = userUp2.id
    }

    let user = users.find((item) => item.id == id )
    user.active = false;

    let index = users.indexOf(user)
    users[index] = {...user};

    let playerSpan = document.getElementById(`players${user.id}`)
    playerSpan.style.color = 'red';

    selectedDisplay = null;
    document.getElementById('modalPlayerNameEdit').value = ''
    $("#modalPlayerCharacterSelectEdit").val('0').trigger('change')
    cleanOptions(true)
    let img = document.getElementById('modalImageCharacterEdit');
    img.src=''
    img.style.display = 'none';
    cleanUserUp(editUser)
    editUser = null;
    modalEdit.style.display = "none";
}

const activateUser = () => {
    id = null;
    if( editUser == 1 ){
        id = userUp1.id
    }else{
        id = userUp2.id
    }

    let user = users.find((item) => item.id == id )
    user.active = true;

    let index = users.indexOf(user)
    users[index] = {...user};

    let playerSpan = document.getElementById(`players${user.id}`)
    playerSpan.style.color = 'white';

    selectedDisplay = null;
    cleanUserUp(editUser)
    editUser = null;
    modalEdit.style.display = "none";
}

const openModalAdd = () => {
    modal.style.display = "block";
}

const handleUser = (i) => {
    let side = 1;
    let num = i+1;
    if(num > 10 ){
        side = 2;
    }
    let user = users.find(( item ) => item.id == num )

    if( side > 1 ){
        userUp2 = user;
    }else{
        userUp1 = user;
    }
    //texto ilegible
    let ilegible = document.getElementById(`ilegible${side}`)
    ilegible.innerHTML = user.nickname

    //texto legible
    let legible = document.getElementById(`legible${side}`)
    legible.innerHTML = user.nickname

    //imagen
    let img = document.getElementById(`charac${side}Image`)
    img.src=`./resources/assets/crew/Chars/${user.character}/Skins/${user.skin}`
    
    ilegible.style.display = 'block'
    legible.style.display = 'block'
    img.style.display = 'block';
}

const cleanUserUp = (num) => {

    if( num > 1 ){
        userUp2 = null;
    }else{
        userUp1 = null;
    }
    //texto ilegible
    let ilegible = document.getElementById(`ilegible${num}`)
    ilegible.innerHTML = ''

    //texto legible
    let legible = document.getElementById(`legible${num}`)
    legible.innerHTML = ''

    //imagen
    let img = document.getElementById(`charac${num}Image`)
    img.src=``
    
    ilegible.style.display = 'none'
    legible.style.display = 'none'
    img.style.display = 'none';
}

const verifyUserStatus = (i, e) => {
    let spans = document.getElementsByClassName('videoNameTag');
    let input = document.getElementById(`playersName${i+1}`)
    selectedDisplay = i+1;
    if(input.value.length < 1) openModalAdd();
    else handleUser(i);
}

const openModalScore = (i, e) => {
    modalScore.style.display = "block";
}

const getSkins = ( directory ) => {
    let char = characters.filter((e) => e.directory == directory)
    if( char.length > 0 ) {
        cleanOptions()
        let selectSkins = document.getElementById('modalSelectSkin');
        let skinsArr = char[0].skins;
        for (var i = 0; i<= skinsArr.length; i++){
            var opt = document.createElement('option');
            if(skinsArr[i]){
                opt.value = skinsArr[i].file;
                opt.innerHTML = skinsArr[i].name;
                selectSkins.appendChild(opt);
            }
        }
    }
}

const getSkinsEdit = ( directory ) => {
    let char = characters.filter((e) => e.directory == directory)
    if( char.length > 0 ) {
        cleanOptions()
        let selectSkins = document.getElementById('modalSelectSkinEdit');
        let skinsArr = char[0].skins;
        for (var i = 0; i<= skinsArr.length; i++){
            var opt = document.createElement('option');
            if(skinsArr[i]){
                opt.value = skinsArr[i].file;
                opt.innerHTML = skinsArr[i].name;
                selectSkins.appendChild(opt);
            }
        }
    }
}

const selectCharacter = ( data ) => {
    let char = characters.filter((e) => e.directory == data.id)
    if( char.length > 0) {
        let info = char[0];
        let img = document.getElementById('modalImageCharacter');
        img.src=`./resources/assets/crew/Chars/${info.directory}/Skins/${info.skins[0].file}`
        img.style.display = 'flex';
        getSkins(info.directory)
    }
   
}

const setSkin = (event) => {
    let e = document.getElementById("modalPlayerCharacterSelect");
    let text = e.options[e.selectedIndex].value;
    let img = document.getElementById('modalImageCharacter');
    img.src=`./resources/assets/crew/Chars/${text}/Skins/${event.target.value}`
    img.style.display = 'flex';
}

const selectCharacterEdit = ( data ) => {
    let char = characters.filter((e) => e.directory == data.id)
    if( char.length > 0) {
        let info = char[0];
        let img = document.getElementById('modalImageCharacterEdit');
        img.src=`./resources/assets/crew/Chars/${info.directory}/Skins/${info.skins[0].file}`
        img.style.display = 'flex';
        getSkinsEdit(info.directory)
    }
}

const setSkinEdit = (event) => {
    let e = document.getElementById("modalPlayerCharacterSelectEdit");
    let text = e.options[e.selectedIndex].value;
    let img = document.getElementById('modalImageCharacterEdit');
    img.src=`./resources/assets/crew/Chars/${text}/Skins/${event.target.value}`
    img.style.display = 'flex';
}
/* FIN  metodos */
