const characters = [
        {
            "id": 1,
            "name": "Banjo and Kazooie",
            "directory": "BanjoAndKazooie",
            "skins": [
                {
                    "name": "Banjo and Kazooie 1",
                    "file": "Banjo_and_Kazooie1.png"
                },
                {
                    "name": "Banjo and Kazooie 2",
                    "file": "Banjo_and_Kazooie2.png"
                },
                {
                    "name": "Banjo and Kazooie 3",
                    "file": "Banjo_and_Kazooie3.png"
                },
                {
                    "name": "Banjo and Kazooie 4",
                    "file": "Banjo_and_Kazooie4.png"
                },
                {
                    "name": "Banjo and Kazooie 5",
                    "file": "Banjo_and_Kazooie5.png"
                },
                {
                    "name": "Banjo and Kazooie 6",
                    "file": "Banjo_and_Kazooie6.png"
                },
                {
                    "name": "Banjo and Kazooie 7",
                    "file": "Banjo_and_Kazooie7.png"
                },
                {
                    "name": "Banjo and Kazooie 8",
                    "file": "Banjo_and_Kazooie8.png"
                },
            ]
        },
        {
            "id": 2,
            "name": "Bowser",
            "directory": "Bowser",
            "skins": [
                {
                    "name": "Bowser 1",
                    "file": "Bowser1.png"
                },
                {
                    "name": "Bowser 2",
                    "file": "Bowser2.png"
                },
                {
                    "name": "Bowser 3",
                    "file": "Bowser3.png"
                },
                {
                    "name": "Bowser 4",
                    "file": "Bowser4.png"
                },
                {
                    "name": "Bowser 5",
                    "file": "Bowser5.png"
                },
                {
                    "name": "Bowser 6",
                    "file": "Bowser6.png"
                },
                {
                    "name": "Bowser 7",
                    "file": "Bowser7.png"
                },
                {
                    "name": "Bowser 8",
                    "file": "Bowser8.png"
                },
            ]
        },
        {
            "id": 3,
            "name": "Bowser jr",
            "directory": "BowserJr",
            "skins": [
                {
                    "name": "Bowser Jr 1",
                    "file": "BowserJr1.png"
                },
                {
                    "name": "Bowser Jr 2",
                    "file": "BowserJr2.png"
                },
                {
                    "name": "Bowser Jr 3",
                    "file": "BowserJr3.png"
                },
                {
                    "name": "Bowser Jr 4",
                    "file": "BowserJr4.png"
                },
                {
                    "name": "Bowser Jr 5",
                    "file": "BowserJr5.png"
                },
                {
                    "name": "Bowser Jr 6",
                    "file": "BowserJr6.png"
                },
                {
                    "name": "Bowser Jr 7",
                    "file": "BowserJr7.png"
                },
                {
                    "name": "Bowser Jr 8",
                    "file": "BowserJr8.png"
                },
            ]
        },
        {
            "id": 4,
            "name": "Byleth",
            "directory": "Byleth",
            "skins": [
                {
                    "name": "Byleth 1",
                    "file": "Byleth1.png"
                },
                {
                    "name": "Byleth 2",
                    "file": "Byleth2.png"
                },
                {
                    "name": "Byleth 3",
                    "file": "Byleth3.png"
                },
                {
                    "name": "Byleth 4",
                    "file": "Byleth4.png"
                },
                {
                    "name": "Byleth 5",
                    "file": "Byleth5.png"
                },
                {
                    "name": "Byleth 6",
                    "file": "Byleth6.png"
                },
                {
                    "name": "Byleth 7",
                    "file": "Byleth7.png"
                },{
                    "name": "Byleth 8",
                    "file": "Byleth8.png"
                },
            ]
        },
        {
            "id": 5,
            "name": "Captain Falcon",
            "directory": "CaptainFalcon",
            "skins": [
                {
                    "name": "Captain Falcon 1",
                    "file": "CaptainFalcon1.png"
                },
                {
                    "name": "Captain Falcon 2",
                    "file": "CaptainFalcon2.png"
                },
                {
                    "name": "Captain Falcon 3",
                    "file": "CaptainFalcon3.png"
                },
                {
                    "name": "Captain Falcon 4",
                    "file": "CaptainFalcon4.png"
                },
                {
                    "name": "Captain Falcon 5",
                    "file": "CaptainFalcon5.png"
                },
                {
                    "name": "Captain Falcon 6",
                    "file": "CaptainFalcon6.png"
                },
                {
                    "name": "Captain Falcon 7",
                    "file": "CaptainFalcon7.png"
                },
                {
                    "name": "Captain Falcon 8",
                    "file": "CaptainFalcon8.png"
                },
            ]
        },
        {
            "id": 6,
            "name": "Chrom",
            "directory": "Chrom",
            "skins": [
                {
                    "name": "Chrom 1",
                    "file": "Chrom1.png"
                },
                {
                    "name": "Chrom 2",
                    "file": "Chrom2.png"
                },
                {
                    "name": "Chrom 3",
                    "file": "Chrom3.png"
                },
                {
                    "name": "Chrom 4",
                    "file": "Chrom4.png"
                },
                {
                    "name": "Chrom 5",
                    "file": "Chrom5.png"
                },
                {
                    "name": "Chrom 6",
                    "file": "Chrom6.png"
                },
                {
                    "name": "Chrom 7",
                    "file": "Chrom7.png"
                },
                {
                    "name": "Chrom 8",
                    "file": "Chrom8.png"
                },
            ]
        },
        {
            "id": 7,
            "name": "Cloud",
            "directory": "Cloud",
            "skins": [
                {
                    "name": "Cloud 1",
                    "file": "Cloud1.png"
                },
                {
                    "name": "Cloud 2",
                    "file": "Cloud2.png"
                },
                {
                    "name": "Cloud 3",
                    "file": "Cloud3.png"
                },
                {
                    "name": "Cloud 4",
                    "file": "Cloud4.png"
                },
                {
                    "name": "Cloud 5",
                    "file": "Cloud5.png"
                },
                {
                    "name": "Cloud 6",
                    "file": "Cloud6.png"
                },
                {
                    "name": "Cloud 7",
                    "file": "Cloud7.png"
                },
                {
                    "name": "Cloud 8",
                    "file": "Cloud8.png"
                },
            ]
        },
        {
            "id": 8,
            "name": "Corrin",
            "directory": "Corrin",
            "skins": [
                {
                    "name": "Corrin 1",
                    "file": "Corrin1.png"
                },
                {
                    "name": "Corrin 2",
                    "file": "Corrin2.png"
                },
                {
                    "name": "Corrin 3",
                    "file": "Corrin3.png"
                },
                {
                    "name": "Corrin 4",
                    "file": "Corrin4.png"
                },
                {
                    "name": "Corrin 5",
                    "file": "Corrin5.png"
                },
                {
                    "name": "Corrin 6",
                    "file": "Corrin6.png"
                },
                {
                    "name": "Corrin 7",
                    "file": "Corrin7.png"
                },
                {
                    "name": "Corrin 8",
                    "file": "Corrin8.png"
                },
            ]
        },{
            "id": 9,
            "name": "Daisy",
            "directory": "Daisy",
            "skins": [
                {
                    "name": "Daisy 1",
                    "file": "Daisy1.png"
                },
                {
                    "name": "Daisy 2",
                    "file": "Daisy2.png"
                },
                {
                    "name": "Daisy 3",
                    "file": "Daisy3.png"
                },
                {
                    "name": "Daisy 4",
                    "file": "Daisy4.png"
                },
                {
                    "name": "Daisy 5",
                    "file": "Daisy5.png"
                },
                {
                    "name": "Daisy 6",
                    "file": "Daisy6.png"
                },
                {
                    "name": "Daisy 7",
                    "file": "Daisy7.png"
                },
                {
                    "name": "Daisy 8",
                    "file": "Daisy8.png"
                },
            ]
        },
        {
            "id": 10,
            "name": "Dark Pit",
            "directory": "DarkPit",
            "skins": [
                {
                    "name": "Dark Pit 1",
                    "file": "DarkPit1.png"
                },
                {
                    "name": "Dark Pit 2",
                    "file": "DarkPit2.png"
                },
                {
                    "name": "Dark Pit 3",
                    "file": "DarkPit3.png"
                },
                {
                    "name": "Dark Pit 4",
                    "file": "DarkPit4.png"
                },
                {
                    "name": "Dark Pit 5",
                    "file": "DarkPit5.png"
                },
                {
                    "name": "Dark Pit 6",
                    "file": "DarkPit6.png"
                },
                {
                    "name": "Dark Pit 7",
                    "file": "DarkPit7.png"
                },
                {
                    "name": "Dark Pit 8",
                    "file": "DarkPit8.png"
                },
            ]
        },
        {
            "id": 11,
            "name": "Dark Samus",
            "directory": "DarkSamus",
            "skins": [
                {
                    "name": "Dark Samus 1",
                    "file": "DarkSamus1.png"
                },
                {
                    "name": "Dark Samus 2",
                    "file": "DarkSamus2.png"
                },
                {
                    "name": "Dark Samus 3",
                    "file": "DarkSamus3.png"
                },
                {
                    "name": "Dark Samus 4",
                    "file": "DarkSamus4.png"
                },
                {
                    "name": "Dark Samus 5",
                    "file": "DarkSamus5.png"
                },
                {
                    "name": "Dark Samus 6",
                    "file": "DarkSamus6.png"
                },
                {
                    "name": "Dark Samus 7",
                    "file": "DarkSamus7.png"
                },
                {
                    "name": "Dark Samus 8",
                    "file": "DarkSamus8.png"
                },
            ]
        },
        {
            "id": 12,
            "name": "Diddy Kong",
            "directory": "DiddyKong",
            "skins": [
                {
                    "name": "Diddy Kong 1",
                    "file": "DiddyKong1.png"
                },
                {
                    "name": "Diddy Kong 2",
                    "file": "DiddyKong2.png"
                },
                {
                    "name": "Diddy Kong 3",
                    "file": "DiddyKong3.png"
                },
                {
                    "name": "Diddy Kong 4",
                    "file": "DiddyKong4.png"
                },
                {
                    "name": "Diddy Kong 5",
                    "file": "DiddyKong5.png"
                },
                {
                    "name": "Diddy Kong 6",
                    "file": "DiddyKong6.png"
                },
                {
                    "name": "Diddy Kong 7",
                    "file": "DiddyKong7.png"
                },
                {
                    "name": "Diddy Kong 8",
                    "file": "DiddyKong8.png"
                },
            ]
        },
        {
            "id": 13,
            "name": "Donkey Kong",
            "directory": "DonkeyKong",
            "skins": [
                {
                    "name": "Donkey Kong 1",
                    "file": "DonkeyKong1.png"
                },
                {
                    "name": "Donkey Kong 2",
                    "file": "DonkeyKong2.png"
                },
                {
                    "name": "Donkey Kong 3",
                    "file": "DonkeyKong3.png"
                },
                {
                    "name": "Donkey Kong 4",
                    "file": "DonkeyKong4.png"
                },
                {
                    "name": "Donkey Kong 5",
                    "file": "DonkeyKong5.png"
                },
                {
                    "name": "Donkey Kong 6",
                    "file": "DonkeyKong6.png"
                },
                {
                    "name": "Donkey Kong 7",
                    "file": "DonkeyKong7.png"
                },
                {
                    "name": "Donkey Kong 8",
                    "file": "DonkeyKong8.png"
                },
            ]
        },
        {
            "id": 14,
            "name": "Dr Mario",
            "directory": "DrMario",
            "skins": [
                {
                    "name": "Dr Mario 1",
                    "file": "DrMario1.png"
                },
                {
                    "name": "Dr Mario 2",
                    "file": "DrMario2.png"
                },
                {
                    "name": "Dr Mario 3",
                    "file": "DrMario3.png"
                },
                {
                    "name": "Dr Mario 4",
                    "file": "DrMario4.png"
                },
                {
                    "name": "Dr Mario 5",
                    "file": "DrMario5.png"
                },
                {
                    "name": "Dr Mario 6",
                    "file": "DrMario6.png"
                },
                {
                    "name": "Dr Mario 7",
                    "file": "DrMario7.png"
                },
                {
                    "name": "Dr Mario 8",
                    "file": "DrMario8.png"
                },
            ]
        },
        {
            "id": 15,
            "name": "Duck Hunt",
            "directory": "DuckHunt",
            "skins": [
                {
                    "name": "Duck Hunt 1",
                    "file": "DuckHunt1.png"
                },
                {
                    "name": "Duck Hunt 2",
                    "file": "DuckHunt2.png"
                },
                {
                    "name": "Duck Hunt 3",
                    "file": "DuckHunt3.png"
                },
                {
                    "name": "Duck Hunt 4",
                    "file": "DuckHunt4.png"
                },
                {
                    "name": "Duck Hunt 5",
                    "file": "DuckHunt5.png"
                },
                {
                    "name": "Duck Hunt 6",
                    "file": "DuckHunt6.png"
                },
                {
                    "name": "Duck Hunt 7",
                    "file": "DuckHunt7.png"
                },
                {
                    "name": "Duck Hunt 8",
                    "file": "DuckHunt8.png"
                },
            ]
        },
        {
            "id": 16,
            "name": "Falco",
            "directory": "Falco",
            "skins": [
                {
                    "name": "Falco 1",
                    "file": "Falco1.png"
                },
                {
                    "name": "Falco 2",
                    "file": "Falco2.png"
                },
                {
                    "name": "Falco 3",
                    "file": "Falco3.png"
                },
                {
                    "name": "Falco 4",
                    "file": "Falco4.png"
                },
                {
                    "name": "Falco 5",
                    "file": "Falco5.png"
                },
                {
                    "name": "Falco 6",
                    "file": "Falco6.png"
                },
                {
                    "name": "Falco 7",
                    "file": "Falco7.png"
                },
                {
                    "name": "Falco 8",
                    "file": "Falco8.png"
                },
            ]
        },
        {
            "id": 17,
            "name": "Fox",
            "directory": "Fox",
            "skins": [
                {
                    "name": "Fox 1",
                    "file": "Fox1.png"
                },
                {
                    "name": "Fox 2",
                    "file": "Fox2.png"
                },
                {
                    "name": "Fox 3",
                    "file": "Fox3.png"
                },
                {
                    "name": "Fox 4",
                    "file": "Fox4.png"
                },
                {
                    "name": "Fox 5",
                    "file": "Fox5.png"
                },
                {
                    "name": "Fox 6",
                    "file": "Fox6.png"
                },
                {
                    "name": "Fox 7",
                    "file": "Fox7.png"
                },
                {
                    "name": "Fox 8",
                    "file": "Fox8.png"
                },
            ]
        },
        {
            "id": 18,
            "name": "Ganondorf",
            "directory": "Ganondorf",
            "skins": [
                {
                    "name": "Ganondorf 1",
                    "file": "Ganondorf1.png"
                },
                {
                    "name": "Ganondorf 2",
                    "file": "Ganondorf2.png"
                },
                {
                    "name": "Ganondorf 3",
                    "file": "Ganondorf3.png"
                },
                {
                    "name": "Ganondorf 4",
                    "file": "Ganondorf4.png"
                },
                {
                    "name": "Ganondorf 5",
                    "file": "Ganondorf5.png"
                },
                {
                    "name": "Ganondorf 6",
                    "file": "Ganondorf6.png"
                },
                {
                    "name": "Ganondorf 7",
                    "file": "Ganondorf7.png"
                },
                {
                    "name": "Ganondorf 8",
                    "file": "Ganondorf8.png"
                },
            ]
        },
        {
            "id": 19,
            "name": "Greninja",
            "directory": "Greninja",
            "skins": [
                {
                    "name": "Greninja 1",
                    "file": "Greninja1.png"
                },
                {
                    "name": "Greninja 2",
                    "file": "Greninja2.png"
                },
                {
                    "name": "Greninja 3",
                    "file": "Greninja3.png"
                },
                {
                    "name": "Greninja 4",
                    "file": "Greninja4.png"
                },
                {
                    "name": "Greninja 5",
                    "file": "Greninja5.png"
                },
                {
                    "name": "Greninja 6",
                    "file": "Greninja6.png"
                },
                {
                    "name": "Greninja 7",
                    "file": "Greninja7.png"
                },
                {
                    "name": "Greninja 8",
                    "file": "Greninja8.png"
                },
            ]
        },
        {
            "id": 20,
            "name": "Hero",
            "directory": "Hero",
            "skins":[
                {
                    "name": "Hero 1",
                    "file": "Hero1.png"
                },
                {
                    "name": "Hero 2",
                    "file": "Hero2.png"
                },
                {
                    "name": "Hero 3",
                    "file": "Hero3.png"
                },
                {
                    "name": "Hero 4",
                    "file": "Hero4.png"
                },
                {
                    "name": "Hero 5",
                    "file": "Hero5.png"
                },
                {
                    "name": "Hero 6",
                    "file": "Hero6.png"
                },
                {
                    "name": "Hero 7",
                    "file": "Hero7.png"
                },
                {
                    "name": "Hero 8",
                    "file": "Hero8.png"
                },
            ]
        },
        {
            "id": 21,
            "name": "Ice Climbers",
            "directory": "IceClimbers",
            "skins":[
                {
                    "name": "IceClimbers 1",
                    "file": "IceClimbers1.png"
                },
                {
                    "name": "IceClimbers 2",
                    "file": "IceClimbers2.png"
                },
                {
                    "name": "IceClimbers 3",
                    "file": "IceClimbers3.png"
                },
                {
                    "name": "IceClimbers 4",
                    "file": "IceClimbers4.png"
                },
                {
                    "name": "IceClimbers 5",
                    "file": "IceClimbers5.png"
                },
                {
                    "name": "IceClimbers 6",
                    "file": "IceClimbers6.png"
                },
                {
                    "name": "IceClimbers 7",
                    "file": "IceClimbers7.png"
                },
                {
                    "name": "IceClimbers 8",
                    "file": "IceClimbers8.png"
                },
            ]
        },
        {
            "id": 22,
            "name": "Ike",
            "directory": "Ike",
            "skins":[
                {
                    "name": "Ike 1",
                    "file": "Ike1.png"
                },
                {
                    "name": "Ike 2",
                    "file": "Ike2.png"
                },
                {
                    "name": "Ike 3",
                    "file": "Ike3.png"
                },
                {
                    "name": "Ike 4",
                    "file": "Ike4.png"
                },
                {
                    "name": "Ike 5",
                    "file": "Ike5.png"
                },
                {
                    "name": "Ike 6",
                    "file": "Ike6.png"
                },
                {
                    "name": "Ike 7",
                    "file": "Ike7.png"
                },
                {
                    "name": "Ike 8",
                    "file": "Ike8.png"
                },
            ]
        },
        {
            "id": 23,
            "name": "Incineroar",
            "directory": "Incineroar",
            "skins":[
                {
                    "name": "Incineroar 1",
                    "file": "Incineroar1.png"
                },
                {
                    "name": "Incineroar 2",
                    "file": "Incineroar2.png"
                },
                {
                    "name": "Incineroar 3",
                    "file": "Incineroar3.png"
                },
                {
                    "name": "Incineroar 4",
                    "file": "Incineroar4.png"
                },
                {
                    "name": "Incineroar 5",
                    "file": "Incineroar5.png"
                },
                {
                    "name": "Incineroar 6",
                    "file": "Incineroar6.png"
                },
                {
                    "name": "Incineroar 7",
                    "file": "Incineroar7.png"
                },
                {
                    "name": "Incineroar 8",
                    "file": "Incineroar8.png"
                },
            ]
        },
        {
            "id": 24,
            "name": "Inkling",
            "directory": "Inkling",
            "skins":[
                {
                    "name": "Inkling 1",
                    "file": "Inkling1.png"
                },
                {
                    "name": "Inkling 2",
                    "file": "Inkling2.png"
                },
                {
                    "name": "Inkling 3",
                    "file": "Inkling3.png"
                },
                {
                    "name": "Inkling 4",
                    "file": "Inkling4.png"
                },
                {
                    "name": "Inkling 5",
                    "file": "Inkling5.png"
                },
                {
                    "name": "Inkling 6",
                    "file": "Inkling6.png"
                },
                {
                    "name": "Inkling 7",
                    "file": "Inkling7.png"
                },
                {
                    "name": "Inkling 8",
                    "file": "Inkling8.png"
                },
            ]
        },
        {
            "id": 25,
            "name": "Isabelle",
            "directory": "Isabelle",
            "skins":[
                {
                    "name": "Isabelle 1",
                    "file": "Isabelle1.png"
                },
                {
                    "name": "Isabelle 2",
                    "file": "Isabelle2.png"
                },
                {
                    "name": "Isabelle 3",
                    "file": "Isabelle3.png"
                },
                {
                    "name": "Isabelle 4",
                    "file": "Isabelle4.png"
                },
                {
                    "name": "Isabelle 5",
                    "file": "Isabelle5.png"
                },
                {
                    "name": "Isabelle 6",
                    "file": "Isabelle6.png"
                },
                {
                    "name": "Isabelle 7",
                    "file": "Isabelle7.png"
                },
                {
                    "name": "Isabelle 8",
                    "file": "Isabelle8.png"
                },
            ]
        },
        {
            "id": 26,
            "name": "Jigglypuff",
            "directory": "Jigglypuff",
            "skins":[
                {
                    "name": "Jigglypuff 1",
                    "file": "Jigglypuff1.png"
                },
                {
                    "name": "Jigglypuff 2",
                    "file": "Jigglypuff2.png"
                },
                {
                    "name": "Jigglypuff 3",
                    "file": "Jigglypuff3.png"
                },
                {
                    "name": "Jigglypuff 4",
                    "file": "Jigglypuff4.png"
                },
                {
                    "name": "Jigglypuff 5",
                    "file": "Jigglypuff5.png"
                },
                {
                    "name": "Jigglypuff 6",
                    "file": "Jigglypuff6.png"
                },
                {
                    "name": "Jigglypuff 7",
                    "file": "Jigglypuff7.png"
                },
                {
                    "name": "Jigglypuff 8",
                    "file": "Jigglypuff8.png"
                },
            ]
        },
        {
            "id": 27,
            "name": "Joker",
            "directory": "Joker",
            "skins":[
                {
                    "name": "Joker 1",
                    "file": "Joker1.png"
                },
                {
                    "name": "Joker 2",
                    "file": "Joker2.png"
                },
                {
                    "name": "Joker 3",
                    "file": "Joker3.png"
                },
                {
                    "name": "Joker 4",
                    "file": "Joker4.png"
                },
                {
                    "name": "Joker 5",
                    "file": "Joker5.png"
                },
                {
                    "name": "Joker 6",
                    "file": "Joker6.png"
                },
                {
                    "name": "Joker 7",
                    "file": "Joker7.png"
                },
                {
                    "name": "Joker 8",
                    "file": "Joker8.png"
                },
            ]
        },
        {
            "id": 28,
            "name": "Kazuya",
            "directory": "Kazuya",
            "skins":[
                {
                    "name": "Kazuya 1",
                    "file": "Kazuya1.png"
                },
                {
                    "name": "Kazuya 2",
                    "file": "Kazuya2.png"
                },
                {
                    "name": "Kazuya 3",
                    "file": "Kazuya3.png"
                },
                {
                    "name": "Kazuya 4",
                    "file": "Kazuya4.png"
                },
                {
                    "name": "Kazuya 5",
                    "file": "Kazuya5.png"
                },
                {
                    "name": "Kazuya 6",
                    "file": "Kazuya6.png"
                },
                {
                    "name": "Kazuya 7",
                    "file": "Kazuya7.png"
                },
                {
                    "name": "Kazuya 8",
                    "file": "Kazuya8.png"
                },
            ]
        },
        {
            "id": 29,
            "name": "Ken",
            "directory": "Ken",
            "skins":[
                {
                    "name": "Ken 1",
                    "file": "Ken1.png"
                },
                {
                    "name": "Ken 2",
                    "file": "Ken2.png"
                },
                {
                    "name": "Ken 3",
                    "file": "Ken3.png"
                },
                {
                    "name": "Ken 4",
                    "file": "Ken4.png"
                },
                {
                    "name": "Ken 5",
                    "file": "Ken5.png"
                },
                {
                    "name": "Ken 6",
                    "file": "Ken6.png"
                },
                {
                    "name": "Ken 7",
                    "file": "Ken7.png"
                },
                {
                    "name": "Ken 8",
                    "file": "Ken8.png"
                },
            ]
        },
        {
            "id": 30,
            "name": "King K Rool",
            "directory": "KingKRool",
            "skins":[
                {
                    "name": "King K Rool 1",
                    "file": "KingKRool1.png"
                },
                {
                    "name": "King K Rool 2",
                    "file": "KingKRool2.png"
                },
                {
                    "name": "King K Rool 3",
                    "file": "KingKRool3.png"
                },
                {
                    "name": "King K Rool 4",
                    "file": "KingKRool4.png"
                },
                {
                    "name": "King K Rool 5",
                    "file": "KingKRool5.png"
                },
                {
                    "name": "King K Rool 6",
                    "file": "KingKRool6.png"
                },
                {
                    "name": "King K Rool 7",
                    "file": "KingKRool7.png"
                },
                {
                    "name": "King K Rool 8",
                    "file": "KingKRool8.png"
                },
            ]
        },
        {
            "id": 31,
            "name": "King Dedede",
            "directory": "KingDedede",
            "skins":[
                {
                    "name": "King Dedede 1",
                    "file": "KingDedede1.png"
                },
                {
                    "name": "King Dedede 2",
                    "file": "KingDedede2.png"
                },
                {
                    "name": "King Dedede 3",
                    "file": "KingDedede3.png"
                },
                {
                    "name": "King Dedede 4",
                    "file": "KingDedede4.png"
                },
                {
                    "name": "King Dedede 5",
                    "file": "KingDedede5.png"
                },
                {
                    "name": "King Dedede 6",
                    "file": "KingDedede6.png"
                },
                {
                    "name": "King Dedede 7",
                    "file": "KingDedede7.png"
                },
                {
                    "name": "King Dedede 8",
                    "file": "KingDedede8.png"
                },
            ]
        },
        {
            "id": 32,
            "name": "Kirby",
            "directory": "Kirby",
            "skins":[
                {
                    "name": "Kirby 1",
                    "file": "Kirby1.png"
                },
                {
                    "name": "Kirby 2",
                    "file": "Kirby2.png"
                },
                {
                    "name": "Kirby 3",
                    "file": "Kirby3.png"
                },
                {
                    "name": "Kirby 4",
                    "file": "Kirby4.png"
                },
                {
                    "name": "Kirby 5",
                    "file": "Kirby5.png"
                },
                {
                    "name": "Kirby 6",
                    "file": "Kirby6.png"
                },
                {
                    "name": "Kirby 7",
                    "file": "Kirby7.png"
                },
                {
                    "name": "Kirby 8",
                    "file": "Kirby8.png"
                },
            ]
        },
        {
            "id": 33,
            "name": "Link",
            "directory": "Link",
            "skins":[
                {
                    "name": "Link 1",
                    "file": "Link1.png"
                },
                {
                    "name": "Link 2",
                    "file": "Link2.png"
                },
                {
                    "name": "Link 3",
                    "file": "Link3.png"
                },
                {
                    "name": "Link 4",
                    "file": "Link4.png"
                },
                {
                    "name": "Link 5",
                    "file": "Link5.png"
                },
                {
                    "name": "Link 6",
                    "file": "Link6.png"
                },
                {
                    "name": "Link 7",
                    "file": "Link7.png"
                },
                {
                    "name": "Link 8",
                    "file": "Link8.png"
                },
            ]
        },
        {
            "id": 34,
            "name": "Little Mac",
            "directory": "LittleMac",
            "skins":[
                {
                    "name": "Little Mac 1",
                    "file": "LittleMac1.png"
                },
                {
                    "name": "Little Mac 2",
                    "file": "LittleMac2.png"
                },
                {
                    "name": "Little Mac 3",
                    "file": "LittleMac3.png"
                },
                {
                    "name": "Little Mac 4",
                    "file": "LittleMac4.png"
                },
                {
                    "name": "Little Mac 5",
                    "file": "LittleMac5.png"
                },
                {
                    "name": "Little Mac 6",
                    "file": "LittleMac6.png"
                },
                {
                    "name": "Little Mac 7",
                    "file": "LittleMac7.png"
                },
                {
                    "name": "Little Mac 8",
                    "file": "LittleMac8.png"
                },
            ]
        },
        {
            "id": 35,
            "name": "Lucario",
            "directory": "Lucario",
            "skins":[
                {
                    "name": "Lucario 1",
                    "file": "Lucario1.png"
                },
                {
                    "name": "Lucario 2",
                    "file": "Lucario2.png"
                },
                {
                    "name": "Lucario 3",
                    "file": "Lucario3.png"
                },
                {
                    "name": "Lucario 4",
                    "file": "Lucario4.png"
                },
                {
                    "name": "Lucario 5",
                    "file": "Lucario5.png"
                },
                {
                    "name": "Lucario 6",
                    "file": "Lucario6.png"
                },
                {
                    "name": "Lucario 7",
                    "file": "Lucario7.png"
                },
                {
                    "name": "Lucario 8",
                    "file": "Lucario8.png"
                },
            ]
        },
        {
            "id": 36,
            "name": "Lucas",
            "directory": "Lucario",
            "skins":[
                {
                    "name": "Lucas 1",
                    "file": "Lucas1.png"
                },
                {
                    "name": "Lucas 2",
                    "file": "Lucas2.png"
                },
                {
                    "name": "Lucas 3",
                    "file": "Lucas3.png"
                },
                {
                    "name": "Lucas 4",
                    "file": "Lucas4.png"
                },
                {
                    "name": "Lucas 5",
                    "file": "Lucas5.png"
                },
                {
                    "name": "Lucas 6",
                    "file": "Lucas6.png"
                },
                {
                    "name": "Lucas 7",
                    "file": "Lucas7.png"
                },
                {
                    "name": "Lucas 8",
                    "file": "Lucas8.png"
                },
            ]
        },
        {
            "id": 37,
            "name": "Lucina",
            "directory": "Lucina",
            "skins":[
                {
                    "name": "Lucina 1",
                    "file": "Lucina1.png"
                },
                {
                    "name": "Lucina 2",
                    "file": "Lucina2.png"
                },
                {
                    "name": "Lucina 3",
                    "file": "Lucina3.png"
                },
                {
                    "name": "Lucina 4",
                    "file": "Lucina4.png"
                },
                {
                    "name": "Lucina 5",
                    "file": "Lucina5.png"
                },
                {
                    "name": "Lucina 6",
                    "file": "Lucina6.png"
                },
                {
                    "name": "Lucina 7",
                    "file": "Lucina7.png"
                },
                {
                    "name": "Lucina 8",
                    "file": "Lucina8.png"
                },
            ]
        },
        {
            "id": 38,
            "name": "Luigi",
            "directory": "Luigi",
            "skins":[
                {
                    "name": "Luigi 1",
                    "file": "Luigi1.png"
                },
                {
                    "name": "Luigi 2",
                    "file": "Luigi2.png"
                },
                {
                    "name": "Luigi 3",
                    "file": "Luigi3.png"
                },
                {
                    "name": "Luigi 4",
                    "file": "Luigi4.png"
                },
                {
                    "name": "Luigi 5",
                    "file": "Luigi5.png"
                },
                {
                    "name": "Luigi 6",
                    "file": "Luigi6.png"
                },
                {
                    "name": "Luigi 7",
                    "file": "Luigi7.png"
                },
                {
                    "name": "Luigi 8",
                    "file": "Luigi8.png"
                },
            ]
        },
        {
            "id": 39,
            "name": "Mario",
            "directory": "Mario",
            "skins":[
                {
                    "name": "Mario 1",
                    "file": "Mario1.png"
                },
                {
                    "name": "Mario 2",
                    "file": "Mario2.png"
                },
                {
                    "name": "Mario 3",
                    "file": "Mario3.png"
                },
                {
                    "name": "Mario 4",
                    "file": "Mario4.png"
                },
                {
                    "name": "Mario 5",
                    "file": "Mario5.png"
                },
                {
                    "name": "Mario 6",
                    "file": "Mario6.png"
                },
                {
                    "name": "Mario 7",
                    "file": "Mario7.png"
                },
                {
                    "name": "Mario 8",
                    "file": "Mario8.png"
                },
            ]
        },
        {
            "id": 40,
            "name": "Marth",
            "directory": "Marth",
            "skins":[
                {
                    "name": "Marth 1",
                    "file": "Marth1.png"
                },
                {
                    "name": "Marth 2",
                    "file": "Marth2.png"
                },
                {
                    "name": "Marth 3",
                    "file": "Marth3.png"
                },
                {
                    "name": "Marth 4",
                    "file": "Marth4.png"
                },
                {
                    "name": "Marth 5",
                    "file": "Marth5.png"
                },
                {
                    "name": "Marth 6",
                    "file": "Marth6.png"
                },
                {
                    "name": "Marth 7",
                    "file": "Marth7.png"
                },
                {
                    "name": "Marth 8",
                    "file": "Marth8.png"
                },
            ]
        },
        {
            "id": 41,
            "name": "Mega Man",
            "directory": "MegaMan",
            "skins":[
                {
                    "name": "Mega Man 1",
                    "file": "MegaMan1.png"
                },
                {
                    "name": "Mega Man 2",
                    "file": "MegaMan2.png"
                },
                {
                    "name": "Mega Man 3",
                    "file": "MegaMan3.png"
                },
                {
                    "name": "Mega Man 4",
                    "file": "MegaMan4.png"
                },
                {
                    "name": "Mega Man 5",
                    "file": "MegaMan5.png"
                },
                {
                    "name": "Mega Man 6",
                    "file": "MegaMan6.png"
                },
                {
                    "name": "Mega Man 7",
                    "file": "MegaMan7.png"
                },
                {
                    "name": "Mega Man 8",
                    "file": "MegaMan8.png"
                },
            ]
        },
        {
            "id": 42,
            "name": "Meta Knight",
            "directory": "MetaKnight",
            "skins":[
                {
                    "name": "Meta Knight 1",
                    "file": "MetaKnight1.png"
                },
                {
                    "name": "Meta Knight 2",
                    "file": "MetaKnight2.png"
                },
                {
                    "name": "Meta Knight 3",
                    "file": "MetaKnight3.png"
                },
                {
                    "name": "Meta Knight 4",
                    "file": "MetaKnight4.png"
                },
                {
                    "name": "Meta Knight 5",
                    "file": "MetaKnight5.png"
                },
                {
                    "name": "Meta Knight 6",
                    "file": "MetaKnight6.png"
                },
                {
                    "name": "Meta Knight 7",
                    "file": "MetaKnight7.png"
                },
                {
                    "name": "Meta Knight 8",
                    "file": "MetaKnight8.png"
                },
            ]
        },
        {
            "id": 43,
            "name": "Mewtwo",
            "directory": "Mewtwo",
            "skins":[
                {
                    "name": "Mewtwo 1",
                    "file": "Mewtwo1.png"
                },
                {
                    "name": "Mewtwo 2",
                    "file": "Mewtwo2.png"
                },
                {
                    "name": "Mewtwo 3",
                    "file": "Mewtwo3.png"
                },
                {
                    "name": "Mewtwo 4",
                    "file": "Mewtwo4.png"
                },
                {
                    "name": "Mewtwo 5",
                    "file": "Mewtwo5.png"
                },
                {
                    "name": "Mewtwo 6",
                    "file": "Mewtwo6.png"
                },
                {
                    "name": "Mewtwo 7",
                    "file": "Mewtwo7.png"
                },
                {
                    "name": "Mewtwo 8",
                    "file": "Mewtwo8.png"
                },
            ]
        },
        {
            "id": 44,
            "name": "Mii Brawler",
            "directory": "MiiBrawler",
            "skins":[
                {
                    "name": "Mii Brawler 1",
                    "file": "MiiBrawler1.png"
                },
                {
                    "name": "Mii Brawler 2",
                    "file": "MiiBrawler2.png"
                },
                {
                    "name": "Mii Brawler 3",
                    "file": "MiiBrawler3.png"
                },
                {
                    "name": "Mii Brawler 4",
                    "file": "MiiBrawler4.png"
                },
            ]
        },
        {
            "id": 45,
            "name": "Mii Gunner",
            "directory": "MiiGunner",
            "skins":[
                {
                    "name": "Mii Gunner 1",
                    "file": "MiiGunner1.png"
                },
                {
                    "name": "Mii Gunner 2",
                    "file": "MiiGunner2.png"
                },
                {
                    "name": "Mii Gunner 3",
                    "file": "MiiGunner3.png"
                },
                {
                    "name": "Mii Gunner 4",
                    "file": "MiiGunner4.png"
                },
            ]
        },
        {
            "id": 46,
            "name": "Mii Swordfighter",
            "directory": "MiiSwordfighter",
            "skins":[
                {
                    "name": "Mii Swordfighter 1",
                    "file": "MiiSwordfighter1.png"
                },
                {
                    "name": "Mii Swordfighter 2",
                    "file": "MiiSwordfighter2.png"
                },
                {
                    "name": "Mii Swordfighter 3",
                    "file": "MiiSwordfighter3.png"
                },
                {
                    "name": "Mii Swordfighter 4",
                    "file": "MiiSwordfighter4.png"
                },
            ]
        },
        {
            "id": 47,
            "name": "Min Min",
            "directory": "MinMin",
            "skins":[
                {
                    "name": "Min Min 1",
                    "file": "MinMin1.png"
                },
                {
                    "name": "Min Min 2",
                    "file": "MinMin2.png"
                },
                {
                    "name": "Min Min 3",
                    "file": "MinMin3.png"
                },
                {
                    "name": "Min Min 4",
                    "file": "MinMin4.png"
                },
                {
                    "name": "Min Min 5",
                    "file": "MinMin5.png"
                },
                {
                    "name": "Min Min 6",
                    "file": "MinMin6.png"
                },
                {
                    "name": "Min Min 7",
                    "file": "MinMin7.png"
                },
                {
                    "name": "Min Min 8",
                    "file": "MinMin8.png"
                },
            ]
        },
        {
            "id": 48,
            "name": "Mr Game and Watch",
            "directory": "MrGameAndWatch",
            "skins":[
                {
                    "name": "Mr Game and Watch 1",
                    "file": "MrGameAndWatch1.png"
                },
                {
                    "name": "Mr Game and Watch 2",
                    "file": "MrGameAndWatch2.png"
                },
                {
                    "name": "Mr Game and Watch 3",
                    "file": "MrGameAndWatch3.png"
                },
                {
                    "name": "Mr Game and Watch 4",
                    "file": "MrGameAndWatch4.png"
                },
                {
                    "name": "Mr Game and Watch 5",
                    "file": "MrGameAndWatch5.png"
                },
                {
                    "name": "Mr Game and Watch 6",
                    "file": "MrGameAndWatch6.png"
                },
                {
                    "name": "Mr Game and Watch 7",
                    "file": "MrGameAndWatch7.png"
                },
                {
                    "name": "Mr Game and Watch 8",
                    "file": "MrGameAndWatch8.png"
                },
            ]
        },
        {
            "id": 49,
            "name": "Mythra",
            "directory": "Mythra",
            "skins":[
                {
                    "name": "Mythra 1",
                    "file": "Mythra1.png"
                },
                {
                    "name": "Mythra 2",
                    "file": "Mythra2.png"
                },
                {
                    "name": "Mythra 3",
                    "file": "Mythra3.png"
                },
                {
                    "name": "Mythra 4",
                    "file": "Mythra4.png"
                },
                {
                    "name": "Mythra 5",
                    "file": "Mythra5.png"
                },
                {
                    "name": "Mythra 6",
                    "file": "Mythra6.png"
                },
                {
                    "name": "Mythra 7",
                    "file": "Mythra7.png"
                },
                {
                    "name": "Mythra 8",
                    "file": "Mythra8.png"
                },
            ]
        },
        {
            "id": 50,
            "name": "Ness",
            "directory": "Ness",
            "skins":[
                {
                    "name": "Ness 1",
                    "file": "Ness1.png"
                },
                {
                    "name": "Ness 2",
                    "file": "Ness2.png"
                },
                {
                    "name": "Ness 3",
                    "file": "Ness3.png"
                },
                {
                    "name": "Ness 4",
                    "file": "Ness4.png"
                },
                {
                    "name": "Ness 5",
                    "file": "Ness5.png"
                },
                {
                    "name": "Ness 6",
                    "file": "Ness6.png"
                },
                {
                    "name": "Ness 7",
                    "file": "Ness7.png"
                },
                {
                    "name": "Ness 8",
                    "file": "Ness8.png"
                },
            ]
        },
        {
            "id": 51,
            "name": "Olimar",
            "directory": "Olimar",
            "skins":[
                {
                    "name": "Olimar 1",
                    "file": "Olimar1.png"
                },
                {
                    "name": "Olimar 2",
                    "file": "Olimar2.png"
                },
                {
                    "name": "Olimar 3",
                    "file": "Olimar3.png"
                },
                {
                    "name": "Olimar 4",
                    "file": "Olimar4.png"
                },
                {
                    "name": "Olimar 5",
                    "file": "Olimar5.png"
                },
                {
                    "name": "Olimar 6",
                    "file": "Olimar6.png"
                },
                {
                    "name": "Olimar 7",
                    "file": "Olimar7.png"
                },
                {
                    "name": "Olimar 8",
                    "file": "Olimar8.png"
                },
            ]
        },
        {
            "id": 52,
            "name": "Pac-Man",
            "directory": "Pac-Man",
            "skins":[
                {
                    "name": "Pac-Man 1",
                    "file": "Pac-Man1.png"
                },
                {
                    "name": "Pac-Man 2",
                    "file": "Pac-Man2.png"
                },
                {
                    "name": "Pac-Man 3",
                    "file": "Pac-Man3.png"
                },
                {
                    "name": "Pac-Man 4",
                    "file": "Pac-Man4.png"
                },
                {
                    "name": "Pac-Man 5",
                    "file": "Pac-Man5.png"
                },
                {
                    "name": "Pac-Man 6",
                    "file": "Pac-Man6.png"
                },
                {
                    "name": "Pac-Man 7",
                    "file": "Pac-Man7.png"
                },
                {
                    "name": "Pac-Man 8",
                    "file": "Pac-Man8.png"
                },
            ]
        },
        {
            "id": 53,
            "name": "Palutena",
            "directory": "Palutena",
            "skins":[
                {
                    "name": "Palutena 1",
                    "file": "Palutena1.png"
                },
                {
                    "name": "Palutena 2",
                    "file": "Palutena2.png"
                },
                {
                    "name": "Palutena 3",
                    "file": "Palutena3.png"
                },
                {
                    "name": "Palutena 4",
                    "file": "Palutena4.png"
                },
                {
                    "name": "Palutena 5",
                    "file": "Palutena5.png"
                },
                {
                    "name": "Palutena 6",
                    "file": "Palutena6.png"
                },
                {
                    "name": "Palutena 7",
                    "file": "Palutena7.png"
                },
                {
                    "name": "Palutena 8",
                    "file": "Palutena8.png"
                },
            ]
        },
        {
            "id": 54,
            "name": "Peach",
            "directory": "Peach",
            "skins":[
                {
                    "name": "Peach 1",
                    "file": "Peach1.png"
                },
                {
                    "name": "Peach 2",
                    "file": "Peach2.png"
                },
                {
                    "name": "Peach 3",
                    "file": "Peach3.png"
                },
                {
                    "name": "Peach 4",
                    "file": "Peach4.png"
                },
                {
                    "name": "Peach 5",
                    "file": "Peach5.png"
                },
                {
                    "name": "Peach 6",
                    "file": "Peach6.png"
                },
                {
                    "name": "Peach 7",
                    "file": "Peach7.png"
                },
                {
                    "name": "Peach 8",
                    "file": "Peach8.png"
                },
            ]
        },
        {
            "id": 55,
            "name": "Pichu",
            "directory": "Pichu",
            "skins":[
                {
                    "name": "Pichu 1",
                    "file": "Pichu1.png"
                },
                {
                    "name": "Pichu 2",
                    "file": "Pichu2.png"
                },
                {
                    "name": "Pichu 3",
                    "file": "Pichu3.png"
                },
                {
                    "name": "Pichu 4",
                    "file": "Pichu4.png"
                },
                {
                    "name": "Pichu 5",
                    "file": "Pichu5.png"
                },
                {
                    "name": "Pichu 6",
                    "file": "Pichu6.png"
                },
                {
                    "name": "Pichu 7",
                    "file": "Pichu7.png"
                },
                {
                    "name": "Pichu 8",
                    "file": "Pichu8.png"
                },
            ]
        },
        {
            "id": 56,
            "name": "Pikachu",
            "directory": "Pikachu",
            "skins":[
                {
                    "name": "Pikachu 1",
                    "file": "Pikachu1.png"
                },
                {
                    "name": "Pikachu 2",
                    "file": "Pikachu2.png"
                },
                {
                    "name": "Pikachu 3",
                    "file": "Pikachu3.png"
                },
                {
                    "name": "Pikachu 4",
                    "file": "Pikachu4.png"
                },
                {
                    "name": "Pikachu 5",
                    "file": "Pikachu5.png"
                },
                {
                    "name": "Pikachu 6",
                    "file": "Pikachu6.png"
                },
                {
                    "name": "Pikachu 7",
                    "file": "Pikachu7.png"
                },
                {
                    "name": "Pikachu 8",
                    "file": "Pikachu8.png"
                },
            ]
        },
        {
            "id": 57,
            "name": "Piranha Plant",
            "directory": "PiranhaPlant",
            "skins":[
                {
                    "name": "Piranha Plant 1",
                    "file": "PiranhaPlant1.png"
                },
                {
                    "name": "Piranha Plant 2",
                    "file": "PiranhaPlant2.png"
                },
                {
                    "name": "Piranha Plant 3",
                    "file": "PiranhaPlant3.png"
                },
                {
                    "name": "Piranha Plant 4",
                    "file": "PiranhaPlant4.png"
                },
                {
                    "name": "Piranha Plant 5",
                    "file": "PiranhaPlant5.png"
                },
                {
                    "name": "Piranha Plant 6",
                    "file": "PiranhaPlant6.png"
                },
                {
                    "name": "Piranha Plant 7",
                    "file": "PiranhaPlant7.png"
                },
                {
                    "name": "Piranha Plant 8",
                    "file": "PiranhaPlant8.png"
                },
            ]
        },
        {
            "id": 58,
            "name": "Pit",
            "directory": "Pit",
            "skins":[
                {
                    "name": "Pit 1",
                    "file": "Pit1.png"
                },
                {
                    "name": "Pit 2",
                    "file": "Pit2.png"
                },
                {
                    "name": "Pit 3",
                    "file": "Pit3.png"
                },
                {
                    "name": "Pit 4",
                    "file": "Pit4.png"
                },
                {
                    "name": "Pit 5",
                    "file": "Pit5.png"
                },
                {
                    "name": "Pit 6",
                    "file": "Pit6.png"
                },
                {
                    "name": "Pit 7",
                    "file": "Pit7.png"
                },
                {
                    "name": "Pit 8",
                    "file": "Pit8.png"
                },
            ]
        },
        {
            "id": 59,
            "name": "Pokemon Trainer",
            "directory": "PokemonTrainer",
            "skins":[
                {
                    "name": "Pokemon Trainer 1",
                    "file": "PokemonTrainer1.png"
                },
                {
                    "name": "Pokemon Trainer 2",
                    "file": "PokemonTrainer2.png"
                },
                {
                    "name": "Pokemon Trainer 3",
                    "file": "PokemonTrainer3.png"
                },
                {
                    "name": "Pokemon Trainer 4",
                    "file": "PokemonTrainer4.png"
                },
                {
                    "name": "Pokemon Trainer 5",
                    "file": "PokemonTrainer5.png"
                },
                {
                    "name": "Pokemon Trainer 6",
                    "file": "PokemonTrainer6.png"
                },
                {
                    "name": "Pokemon Trainer 7",
                    "file": "PokemonTrainer7.png"
                },
                {
                    "name": "Pokemon Trainer 8",
                    "file": "PokemonTrainer8.png"
                },
            ]
        },
        {
            "id": 60,
            "name": "Pyra",
            "directory": "Pyra",
            "skins":[
                {
                    "name": "Pyra 1",
                    "file": "Pyra1.png"
                },
                {
                    "name": "Pyra 2",
                    "file": "Pyra2.png"
                },
                {
                    "name": "Pyra 3",
                    "file": "Pyra3.png"
                },
                {
                    "name": "Pyra 4",
                    "file": "Pyra4.png"
                },
                {
                    "name": "Pyra 5",
                    "file": "Pyra5.png"
                },
                {
                    "name": "Pyra 6",
                    "file": "Pyra6.png"
                },
                {
                    "name": "Pyra 7",
                    "file": "Pyra7.png"
                },
                {
                    "name": "Pyra 8",
                    "file": "Pyra8.png"
                },
            ]
        },
        {
            "id": 61,
            "name": "Random",
            "directory": "Random",
            "skins":[
                {
                    "name": "Random 1",
                    "file": "Random1.png"
                },
                {
                    "name": "Random 2",
                    "file": "Random2.png"
                },
                {
                    "name": "Random 3",
                    "file": "Random3.png"
                },
                {
                    "name": "Random 4",
                    "file": "Random4.png"
                },
            ]
        },
        {
            "id": 62,
            "name": "Ritcher",
            "directory": "Ritcher",
            "skins":[
                {
                    "name": "Ritcher 1",
                    "file": "Ritcher1.png"
                },
                {
                    "name": "Ritcher 2",
                    "file": "Ritcher2.png"
                },
                {
                    "name": "Ritcher 3",
                    "file": "Ritcher3.png"
                },
                {
                    "name": "Ritcher 4",
                    "file": "Ritcher4.png"
                },
                {
                    "name": "Ritcher 5",
                    "file": "Ritcher5.png"
                },
                {
                    "name": "Ritcher 6",
                    "file": "Ritcher6.png"
                },
                {
                    "name": "Ritcher 7",
                    "file": "Ritcher7.png"
                },
                {
                    "name": "Ritcher 8",
                    "file": "Ritcher8.png"
                },
            ]
        },
        {
            "id": 63,
            "name": "Ridley",
            "directory": "Ridley",
            "skins":[
                {
                    "name": "Ridley 1",
                    "file": "Ridley1.png"
                },
                {
                    "name": "Ridley 2",
                    "file": "Ridley2.png"
                },
                {
                    "name": "Ridley 3",
                    "file": "Ridley3.png"
                },
                {
                    "name": "Ridley 4",
                    "file": "Ridley4.png"
                },
                {
                    "name": "Ridley 5",
                    "file": "Ridley5.png"
                },
                {
                    "name": "Ridley 6",
                    "file": "Ridley6.png"
                },
                {
                    "name": "Ridley 7",
                    "file": "Ridley7.png"
                },
                {
                    "name": "Ridley 8",
                    "file": "Ridley8.png"
                },
            ]
        },
        {
            "id": 64,
            "name": "ROB",
            "directory": "ROB",
            "skins":[
                {
                    "name": "ROB 1",
                    "file": "ROB1.png"
                },
                {
                    "name": "ROB 2",
                    "file": "ROB2.png"
                },
                {
                    "name": "ROB 3",
                    "file": "ROB3.png"
                },
                {
                    "name": "ROB 4",
                    "file": "ROB4.png"
                },
                {
                    "name": "ROB 5",
                    "file": "ROB5.png"
                },
                {
                    "name": "ROB 6",
                    "file": "ROB6.png"
                },
                {
                    "name": "ROB 7",
                    "file": "ROB7.png"
                },
                {
                    "name": "ROB 8",
                    "file": "ROB8.png"
                },
            ]
        },
        {
            "id": 65,
            "name": "Robin",
            "directory": "Robin",
            "skins":[
                {
                    "name": "Robin 1",
                    "file": "Robin1.png"
                },
                {
                    "name": "Robin 2",
                    "file": "Robin2.png"
                },
                {
                    "name": "Robin 3",
                    "file": "Robin3.png"
                },
                {
                    "name": "Robin 4",
                    "file": "Robin4.png"
                },
                {
                    "name": "Robin 5",
                    "file": "Robin5.png"
                },
                {
                    "name": "Robin 6",
                    "file": "Robin6.png"
                },
                {
                    "name": "Robin 7",
                    "file": "Robin7.png"
                },
                {
                    "name": "Robin 8",
                    "file": "Robin8.png"
                },
            ]
        },
        {
            "id": 66,
            "name": "Rosalina and Luma",
            "directory": "RosalinaAndLuma",
            "skins":[
                {
                    "name": "Rosalina and Luma 1",
                    "file": "RosalinaAndLuma1.png"
                },
                {
                    "name": "Rosalina and Luma 2",
                    "file": "RosalinaAndLuma2.png"
                },
                {
                    "name": "Rosalina and Luma 3",
                    "file": "RosalinaAndLuma3.png"
                },
                {
                    "name": "Rosalina and Luma 4",
                    "file": "RosalinaAndLuma4.png"
                },
                {
                    "name": "Rosalina and Luma 5",
                    "file": "RosalinaAndLuma5.png"
                },
                {
                    "name": "Rosalina and Luma 6",
                    "file": "RosalinaAndLuma6.png"
                },
                {
                    "name": "Rosalina and Luma 7",
                    "file": "RosalinaAndLuma7.png"
                },
                {
                    "name": "Rosalina and Luma 8",
                    "file": "RosalinaAndLuma8.png"
                },
            ]
        },
        {
            "id": 67,
            "name": "Roy",
            "directory": "Roy",
            "skins":[
                {
                    "name": "Roy 1",
                    "file": "Roy1.png"
                },
                {
                    "name": "Roy 2",
                    "file": "Roy2.png"
                },
                {
                    "name": "Roy 3",
                    "file": "Roy3.png"
                },
                {
                    "name": "Roy 4",
                    "file": "Roy4.png"
                },
                {
                    "name": "Roy 5",
                    "file": "Roy5.png"
                },
                {
                    "name": "Roy 6",
                    "file": "Roy6.png"
                },
                {
                    "name": "Roy 7",
                    "file": "Roy7.png"
                },
                {
                    "name": "Roy 8",
                    "file": "Roy8.png"
                },
            ]
        },
        {
            "id": 68,
            "name": "Ryu",
            "directory": "Ryu",
            "skins":[
                {
                    "name": "Ryu 1",
                    "file": "Ryu1.png"
                },
                {
                    "name": "Ryu 2",
                    "file": "Ryu2.png"
                },
                {
                    "name": "Ryu 3",
                    "file": "Ryu3.png"
                },
                {
                    "name": "Ryu 4",
                    "file": "Ryu4.png"
                },
                {
                    "name": "Ryu 5",
                    "file": "Ryu5.png"
                },
                {
                    "name": "Ryu 6",
                    "file": "Ryu6.png"
                },
                {
                    "name": "Ryu 7",
                    "file": "Ryu7.png"
                },
                {
                    "name": "Ryu 8",
                    "file": "Ryu8.png"
                },
            ]
        },
        {
            "id": 69,
            "name": "Samus",
            "directory": "Samus",
            "skins":[
                {
                    "name": "Samus 1",
                    "file": "Samus1.png"
                },
                {
                    "name": "Samus 2",
                    "file": "Samus2.png"
                },
                {
                    "name": "Samus 3",
                    "file": "Samus3.png"
                },
                {
                    "name": "Samus 4",
                    "file": "Samus4.png"
                },
                {
                    "name": "Samus 5",
                    "file": "Samus5.png"
                },
                {
                    "name": "Samus 6",
                    "file": "Samus6.png"
                },
                {
                    "name": "Samus 7",
                    "file": "Samus7.png"
                },
                {
                    "name": "Samus 8",
                    "file": "Samus8.png"
                },
            ]
        },
        {
            "id": 70,
            "name": "Sephiroth",
            "directory": "Sephiroth",
            "skins":[
                {
                    "name": "Sephiroth 1",
                    "file": "Sephiroth1.png"
                },
                {
                    "name": "Sephiroth 2",
                    "file": "Sephiroth2.png"
                },
                {
                    "name": "Sephiroth 3",
                    "file": "Sephiroth3.png"
                },
                {
                    "name": "Sephiroth 4",
                    "file": "Sephiroth4.png"
                },
                {
                    "name": "Sephiroth 5",
                    "file": "Sephiroth5.png"
                },
                {
                    "name": "Sephiroth 6",
                    "file": "Sephiroth6.png"
                },
                {
                    "name": "Sephiroth 7",
                    "file": "Sephiroth7.png"
                },
                {
                    "name": "Sephiroth 8",
                    "file": "Sephiroth8.png"
                },
            ]
        },
        {
            "id": 71,
            "name": "Sheik",
            "directory": "Sheik",
            "skins":[
                {
                    "name": "Sheik 1",
                    "file": "Sheik1.png"
                },
                {
                    "name": "Sheik 2",
                    "file": "Sheik2.png"
                },
                {
                    "name": "Sheik 3",
                    "file": "Sheik3.png"
                },
                {
                    "name": "Sheik 4",
                    "file": "Sheik4.png"
                },
                {
                    "name": "Sheik 5",
                    "file": "Sheik5.png"
                },
                {
                    "name": "Sheik 6",
                    "file": "Sheik6.png"
                },
                {
                    "name": "Sheik 7",
                    "file": "Sheik7.png"
                },
                {
                    "name": "Sheik 8",
                    "file": "Sheik8.png"
                },
            ]
        },
        {
            "id": 72,
            "name": "Shulk",
            "directory": "Shulk",
            "skins":[
                {
                    "name": "Shulk 1",
                    "file": "Shulk1.png"
                },
                {
                    "name": "Shulk 2",
                    "file": "Shulk2.png"
                },
                {
                    "name": "Shulk 3",
                    "file": "Shulk3.png"
                },
                {
                    "name": "Shulk 4",
                    "file": "Shulk4.png"
                },
                {
                    "name": "Shulk 5",
                    "file": "Shulk5.png"
                },
                {
                    "name": "Shulk 6",
                    "file": "Shulk6.png"
                },
                {
                    "name": "Shulk 7",
                    "file": "Shulk7.png"
                },
                {
                    "name": "Shulk 8",
                    "file": "Shulk8.png"
                },
            ]
        },
        {
            "id": 73,
            "name": "Simon",
            "directory": "Simon",
            "skins":[
                {
                    "name": "Simon 1",
                    "file": "Simon1.png"
                },
                {
                    "name": "Simon 2",
                    "file": "Simon2.png"
                },
                {
                    "name": "Simon 3",
                    "file": "Simon3.png"
                },
                {
                    "name": "Simon 4",
                    "file": "Simon4.png"
                },
                {
                    "name": "Simon 5",
                    "file": "Simon5.png"
                },
                {
                    "name": "Simon 6",
                    "file": "Simon6.png"
                },
                {
                    "name": "Simon 7",
                    "file": "Simon7.png"
                },
                {
                    "name": "Simon 8",
                    "file": "Simon8.png"
                },
            ]
        },
        {
            "id": 74,
            "name": "Snake",
            "directory": "Snake",
            "skins":[
                {
                    "name": "Snake 1",
                    "file": "Snake1.png"
                },
                {
                    "name": "Snake 2",
                    "file": "Snake2.png"
                },
                {
                    "name": "Snake 3",
                    "file": "Snake3.png"
                },
                {
                    "name": "Snake 4",
                    "file": "Snake4.png"
                },
                {
                    "name": "Snake 5",
                    "file": "Snake5.png"
                },
                {
                    "name": "Snake 6",
                    "file": "Snake6.png"
                },
                {
                    "name": "Snake 7",
                    "file": "Snake7.png"
                },
                {
                    "name": "Snake 8",
                    "file": "Snake8.png"
                },
            ]
        },
        {
            "id": 75,
            "name": "Sonic",
            "directory": "Sonic",
            "skins":[
                {
                    "name": "Sonic 1",
                    "file": "Sonic1.png"
                },
                {
                    "name": "Sonic 2",
                    "file": "Sonic2.png"
                },
                {
                    "name": "Sonic 3",
                    "file": "Sonic3.png"
                },
                {
                    "name": "Sonic 4",
                    "file": "Sonic4.png"
                },
                {
                    "name": "Sonic 5",
                    "file": "Sonic5.png"
                },
                {
                    "name": "Sonic 6",
                    "file": "Sonic6.png"
                },
                {
                    "name": "Sonic 7",
                    "file": "Sonic7.png"
                },
                {
                    "name": "Sonic 8",
                    "file": "Sonic8.png"
                },
            ]
        },
        {
            "id": 76,
            "name": "Sora",
            "directory": "Sora",
            "skins":[
                {
                    "name": "Sora 1",
                    "file": "Sora1.png"
                },
                {
                    "name": "Sora 2",
                    "file": "Sora2.png"
                },
                {
                    "name": "Sora 3",
                    "file": "Sora3.png"
                },
                {
                    "name": "Sora 4",
                    "file": "Sora4.png"
                },
                {
                    "name": "Sora 5",
                    "file": "Sora5.png"
                },
                {
                    "name": "Sora 6",
                    "file": "Sora6.png"
                },
                {
                    "name": "Sora 7",
                    "file": "Sora7.png"
                },
                {
                    "name": "Sora 8",
                    "file": "Sora8.png"
                },
            ]
        },
        {
            "id": 77,
            "name": "Terry",
            "directory": "Terry",
            "skins":[
                {
                    "name": "Terry 1",
                    "file": "Terry1.png"
                },
                {
                    "name": "Terry 2",
                    "file": "Terry2.png"
                },
                {
                    "name": "Terry 3",
                    "file": "Terry3.png"
                },
                {
                    "name": "Terry 4",
                    "file": "Terry4.png"
                },
                {
                    "name": "Terry 5",
                    "file": "Terry5.png"
                },
                {
                    "name": "Terry 6",
                    "file": "Terry6.png"
                },
                {
                    "name": "Terry 7",
                    "file": "Terry7.png"
                },
                {
                    "name": "Terry 8",
                    "file": "Terry8.png"
                },
            ]
        },
        {
            "id": 78,
            "name": "Toon Link",
            "directory": "ToonLink",
            "skins":[
                {
                    "name": "Toon Link 1",
                    "file": "ToonLink1.png"
                },
                {
                    "name": "Toon Link 2",
                    "file": "ToonLink2.png"
                },
                {
                    "name": "Toon Link 3",
                    "file": "ToonLink3.png"
                },
                {
                    "name": "Toon Link 4",
                    "file": "ToonLink4.png"
                },
                {
                    "name": "Toon Link 5",
                    "file": "ToonLink5.png"
                },
                {
                    "name": "Toon Link 6",
                    "file": "ToonLink6.png"
                },
                {
                    "name": "Toon Link 7",
                    "file": "ToonLink7.png"
                },
                {
                    "name": "Toon Link 8",
                    "file": "ToonLink8.png"
                },
            ]
        },
        // {
        //     "id": 79,
        //     "name": "Trails",
        //     "directory": "Trails",
        //     "skins":[
        //         {
        //             "name": "Trails 1",
        //             "file": "Trails1.png"
        //         },
        //         {
        //             "name": "Trails 2",
        //             "file": "Trails2.png"
        //         },
        //         {
        //             "name": "Trails 3",
        //             "file": "Trails3.png"
        //         },
        //         {
        //             "name": "Trails 4",
        //             "file": "Trails4.png"
        //         },
        //         {
        //             "name": "Trails 5",
        //             "file": "Trails5.png"
        //         },
        //         {
        //             "name": "Trails 6",
        //             "file": "Trails6.png"
        //         },
        //         {
        //             "name": "Trails 7",
        //             "file": "Trails7.png"
        //         },
        //         {
        //             "name": "Trails 8",
        //             "file": "Trails8.png"
        //         },
        //     ]
        // },
        {
            "id": 79,
            "name": "Villager",
            "directory": "Villager",
            "skins":[
                {
                    "name": "Villager 1",
                    "file": "Villager1.png"
                },
                {
                    "name": "Villager 2",
                    "file": "Villager2.png"
                },
                {
                    "name": "Villager 3",
                    "file": "Villager3.png"
                },
                {
                    "name": "Villager 4",
                    "file": "Villager4.png"
                },
                {
                    "name": "Villager 5",
                    "file": "Villager5.png"
                },
                {
                    "name": "Villager 6",
                    "file": "Villager6.png"
                },
                {
                    "name": "Villager 7",
                    "file": "Villager7.png"
                },
                {
                    "name": "Villager 8",
                    "file": "Villager8.png"
                },
            ]
        },
        {
            "id": 80,
            "name": "Wario",
            "directory": "Wario",
            "skins":[
                {
                    "name": "Wario 1",
                    "file": "Wario1.png"
                },
                {
                    "name": "Wario 2",
                    "file": "Wario2.png"
                },
                {
                    "name": "Wario 3",
                    "file": "Wario3.png"
                },
                {
                    "name": "Wario 4",
                    "file": "Wario4.png"
                },
                {
                    "name": "Wario 5",
                    "file": "Wario5.png"
                },
                {
                    "name": "Wario 6",
                    "file": "Wario6.png"
                },
                {
                    "name": "Wario 7",
                    "file": "Wario7.png"
                },
                {
                    "name": "Wario 8",
                    "file": "Wario8.png"
                },
            ]
        },
        {
            "id": 81,
            "name": "Wii Fit Trainer",
            "directory": "WiiFitTrainer",
            "skins":[
                {
                    "name": "Wii Fit Trainer 1",
                    "file": "WiiFitTrainer1.png"
                },
                {
                    "name": "Wii Fit Trainer 2",
                    "file": "WiiFitTrainer2.png"
                },
                {
                    "name": "Wii Fit Trainer 3",
                    "file": "WiiFitTrainer3.png"
                },
                {
                    "name": "Wii Fit Trainer 4",
                    "file": "WiiFitTrainer4.png"
                },
                {
                    "name": "Wii Fit Trainer 5",
                    "file": "WiiFitTrainer5.png"
                },
                {
                    "name": "Wii Fit Trainer 6",
                    "file": "WiiFitTrainer6.png"
                },
                {
                    "name": "Wii Fit Trainer 7",
                    "file": "WiiFitTrainer7.png"
                },
                {
                    "name": "Wii Fit Trainer 8",
                    "file": "WiiFitTrainer8.png"
                },
            ]
        },
        {
            "id": 82,
            "name": "Wolf",
            "directory": "Wolf",
            "skins":[
                {
                    "name": "Wolf 1",
                    "file": "Wolf1.png"
                },
                // {
                //     "name": "Wolf 2",
                //     "file": "Wolf2.png"
                // },
                {
                    "name": "Wolf 3",
                    "file": "Wolf3.png"
                },
                {
                    "name": "Wolf 4",
                    "file": "Wolf4.png"
                },
                {
                    "name": "Wolf 5",
                    "file": "Wolf5.png"
                },
                {
                    "name": "Wolf 6",
                    "file": "Wolf6.png"
                },
                {
                    "name": "Wolf 7",
                    "file": "Wolf7.png"
                },
                {
                    "name": "Wolf 8",
                    "file": "Wolf8.png"
                },
            ]
        },
        {
            "id": 83,
            "name": "Yoshi",
            "directory": "Yoshi",
            "skins":[
                {
                    "name": "Yoshi 1",
                    "file": "Yoshi1.png"
                },
                {
                    "name": "Yoshi 2",
                    "file": "Yoshi2.png"
                },
                {
                    "name": "Yoshi 3",
                    "file": "Yoshi3.png"
                },
                {
                    "name": "Yoshi 4",
                    "file": "Yoshi4.png"
                },
                {
                    "name": "Yoshi 5",
                    "file": "Yoshi5.png"
                },
                {
                    "name": "Yoshi 6",
                    "file": "Yoshi6.png"
                },
                {
                    "name": "Yoshi 7",
                    "file": "Yoshi7.png"
                },
                {
                    "name": "Yoshi 8",
                    "file": "Yoshi8.png"
                },
            ]
        },
        {
            "id": 84,
            "name": "Young Link",
            "directory": "YoungLink",
            "skins":[
                {
                    "name": "Young Link 1",
                    "file": "YoungLink1.png"
                },
                {
                    "name": "Young Link 2",
                    "file": "YoungLink2.png"
                },
                {
                    "name": "Young Link 3",
                    "file": "YoungLink3.png"
                },
                {
                    "name": "Young Link 4",
                    "file": "YoungLink4.png"
                },
                {
                    "name": "Young Link 5",
                    "file": "YoungLink5.png"
                },
                {
                    "name": "Young Link 6",
                    "file": "YoungLink6.png"
                },
                {
                    "name": "Young Link 7",
                    "file": "YoungLink7.png"
                },
                {
                    "name": "Young Link 8",
                    "file": "YoungLink8.png"
                },
            ]
        },
        {
            "id": 85,
            "name": "Zelda",
            "directory": "Zelda",
            "skins":[
                {
                    "name": "Zelda 1",
                    "file": "Zelda1.png"
                },
                {
                    "name": "Zelda 2",
                    "file": "Zelda2.png"
                },
                {
                    "name": "Zelda 3",
                    "file": "Zelda3.png"
                },
                {
                    "name": "Zelda 4",
                    "file": "Zelda4.png"
                },
                {
                    "name": "Zelda 5",
                    "file": "Zelda5.png"
                },
                {
                    "name": "Zelda 6",
                    "file": "Zelda6.png"
                },
                {
                    "name": "Zelda 7",
                    "file": "Zelda7.png"
                },
                {
                    "name": "Zelda 8",
                    "file": "Zelda8.png"
                },
            ]
        },
        {
            "id": 86,
            "name": "Zero Suit Samus",
            "directory": "ZeroSuitSamus",
            "skins":[
                {
                    "name": "Zero Suit Samus 1",
                    "file": "ZeroSuitSamus1.png"
                },
                {
                    "name": "Zero Suit Samus 2",
                    "file": "ZeroSuitSamus2.png"
                },
                {
                    "name": "Zero Suit Samus 3",
                    "file": "ZeroSuitSamus3.png"
                },
                {
                    "name": "Zero Suit Samus 4",
                    "file": "ZeroSuitSamus4.png"
                },
                {
                    "name": "Zero Suit Samus 5",
                    "file": "ZeroSuitSamus5.png"
                },
                {
                    "name": "Zero Suit Samus 6",
                    "file": "ZeroSuitSamus6.png"
                },
                {
                    "name": "Zero Suit Samus 7",
                    "file": "ZeroSuitSamus7.png"
                },
                {
                    "name": "Zero Zuit Samus 8",
                    "file": "ZeroSuitSamus8.png"
                },
            ]
        }
    ]

let select = document.getElementById('modalPlayerCharacterSelect');
let selectEdit = document.getElementById('modalPlayerCharacterSelectEdit');

for (var i = 0; i<=characters.length; i++){
        var opt = document.createElement('option');
        if(characters[i]){
            opt.value = characters[i].directory;
            opt.innerHTML = characters[i].name;
            select.appendChild(opt);
        }
}


for (var i = 0; i<=characters.length; i++){
    var opt = document.createElement('option');
    if(characters[i]){
        opt.value = characters[i].directory;
        opt.innerHTML = characters[i].name;
        selectEdit.appendChild(opt);
    }
}

console.log(select,'select')
console.log(selectEdit)